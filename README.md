# README #

This is an iOS universal app demo.

* Structured in MVC pattern.
* CocoaPods based.
* Fetchs data from Kiva web-service using AFNetworking.
* Store data persistently with Realm.
* Shows data from the webservice in a UITableViewController, with pull-to-refresh feature.
* Detail view of every loan entry.
* Storyboard + AutoLayout.
* Reachability in order to handle Internet connection.

![screenshot2.png](https://bitbucket.org/repo/ky4A8z/images/1275678541-screenshot2.png)
![screenshot1.png](https://bitbucket.org/repo/ky4A8z/images/2508741544-screenshot1.png)