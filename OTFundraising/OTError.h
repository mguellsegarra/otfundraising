//
//  OTError.h
//  OTFundraising
//
//  Created by Marc Güell Segarra on 16/10/15.
//  Copyright © 2015 Marc Güell Segarra. All rights reserved.
//

#import <Foundation/Foundation.h>

#define CODE_GENERIC_ERROR 901

@interface OTError : NSError

+ (instancetype)errorWithText:(NSString *)errorKey;

@end
