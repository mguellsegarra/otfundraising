//
//  DataController.m
//  OTFundraising
//
//  Created by Marc Güell Segarra on 16/10/15.
//  Copyright © 2015 Marc Güell Segarra. All rights reserved.
//

#import "DataController.h"
#import "APIManager.h"
#import "OTError.h"
#import "OTLoanLocation.h"
#include <stdlib.h>

@implementation DataController {
	RLMRealm *_realm;
}

-(id)initWithRealm:(RLMRealm *)realm {
	if ((self = [super init]))
	{
		_realm = realm;
	}
	return self;
}

- (void)fetchData:(ErrorBlock)block {
	if([[self getAllLoans] count] > 0) {
		[self clearRealmDatabase];
	}
	
	[self parseLoans:block];
}

- (void)parseLoans:(ErrorBlock)block {
	[APIManager fetchDataFromRestfulWS:^(NSError *error, id object) {
		
		// Let's check if the server response is valid and the object is a dictionary
		if(![object isKindOfClass:[NSDictionary class]]) {
			block([OTError errorWithText:@"WS Error: Object is not a dictionary"]);
			return;
		}
		
		NSDictionary *serverResponse = (NSDictionary *)object;
		
		// Check for "loans" node in JSON response
		if ([serverResponse[@"loans"] isEqual:[NSNull null]]) {
			block([OTError errorWithText:@"WS Error: There's no loans entry in server response"]);
			return;
		}
		
		// Check if the "loans" node is an array
		if(![serverResponse[@"loans"] isKindOfClass:[NSArray class]]) {
			block([OTError errorWithText:@"WS Error: loans node isn´t an array"]);
			return;
		}
		
		NSArray *loansArray = (NSArray *)serverResponse[@"loans"];
		
		for (NSDictionary *loanDict in loansArray) {
			[self createLoanFromDictionary:loanDict];
		}
		
		block(nil);
	}];
}

- (void)createLoanFromDictionary:(NSDictionary *)loanDict {
	OTLoan *loan = [[OTLoan alloc] init];
	loan.name = loanDict[@"name"];
	loan.status = loanDict[@"status"];
	loan.sector = loanDict[@"sector"];
	loan.activity = loanDict[@"activity"];
	loan.location = [self createLocationFromDictionary:loanDict[@"location"]];
	loan.isGreen = [self getRandomBool];
	
	RLMRealm *realm = [RLMRealm defaultRealm];
	
	[realm beginWriteTransaction];
	[realm addObject:loan];
	[realm commitWriteTransaction];
}

- (OTLoanLocation *)createLocationFromDictionary:(NSDictionary *)locationDict {
	OTLoanLocation *location = [[OTLoanLocation alloc] init];
	location.countryCode = locationDict[@"country_code"];
	location.country = locationDict[@"country"];
	location.town = locationDict[@"town"];

	RLMRealm *realm = [RLMRealm defaultRealm];
	
	[realm beginWriteTransaction];
	[realm addObject:location];
	[realm commitWriteTransaction];
	
	return location;
}

- (RLMResults<OTLoan *> *)getAllLoans {
	return [OTLoan allObjects];
}

- (void)clearRealmDatabase {
	RLMRealm *realm = [RLMRealm defaultRealm];

	[realm beginWriteTransaction];
	[realm deleteAllObjects];
	[realm commitWriteTransaction];
}

- (BOOL)getRandomBool {
	int tmp = arc4random_uniform(100);
	if(tmp % 2 == 0)
		return YES;
	return NO;
}

@end