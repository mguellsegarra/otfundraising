//
//  LoanTableViewCell.h
//  OTFundraising
//
//  Created by Marc Güell Segarra on 16/10/15.
//  Copyright © 2015 Marc Güell Segarra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoanTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *loanNameLabel;

@end
