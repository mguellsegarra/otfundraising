//
//  APIManager.m
//  OTFundraising
//
//  Created by Marc Güell Segarra on 16/10/15.
//  Copyright © 2015 Marc Güell Segarra. All rights reserved.
//

#import "APIManager.h"

@implementation APIManager

+ (void)fetchDataFromRestfulWS:(CompletionBlock)block {
	NSURL *myUrl = [NSURL URLWithString:kRestUrl];
	NSURLRequest *request = [[NSURLRequest alloc] initWithURL:myUrl cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10];

	AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
	operation.responseSerializer = [AFJSONResponseSerializer serializer];

	[operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
		block(nil, responseObject);
	} failure:^(AFHTTPRequestOperation *operation, NSError *error){
		block(error, nil);
	}];
	
	[operation start];
}

@end