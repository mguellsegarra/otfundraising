//
//  Blocks.h
//  OTFundraising
//
//  Created by Marc Güell Segarra on 16/10/15.
//  Copyright © 2015 Marc Güell Segarra. All rights reserved.
//

#ifndef Blocks_h
#define Blocks_h

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)

/**
 *  Common Error block
 *
 *  @param error    error or nil
 */
typedef void (^ErrorBlock)(NSError *error);

/**
 *  Common Completion block
 *
 *  @param error    error or nil
 *  @param object   object returned
 */
typedef void (^CompletionBlock)(NSError *error, id object);

/**
 *  Simple block
 */
typedef void(^SimpleBlock)();

#endif /* Blocks_h */
