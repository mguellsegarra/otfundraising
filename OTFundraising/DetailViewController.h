//
//  DetailViewController.h
//  OTFundraising
//
//  Created by Marc Güell Segarra on 16/10/15.
//  Copyright © 2015 Marc Güell Segarra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OTLoan.h"

@interface DetailViewController : UIViewController

@property OTLoan *loan;

@end
