//
//  OTError.m
//  OTFundraising
//
//  Created by Marc Güell Segarra on 16/10/15.
//  Copyright © 2015 Marc Güell Segarra. All rights reserved.
//

#import "OTError.h"

@implementation OTError

+ (instancetype)errorWithText:(NSString *)errorText withCode:(NSInteger)code {
	NSDictionary *details = @{NSLocalizedDescriptionKey : errorText};
	OTError *error = [OTError errorWithDomain:@"OTFundraising" code:code userInfo:details];
	return error;
}

+ (instancetype)errorWithText:(NSString *)errorText {
	return [self errorWithText:errorText withCode:CODE_GENERIC_ERROR];
}

@end
