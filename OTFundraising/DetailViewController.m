//
//  DetailViewController.m
//  OTFundraising
//
//  Created by Marc Güell Segarra on 16/10/15.
//  Copyright © 2015 Marc Güell Segarra. All rights reserved.
//

#import "DetailViewController.h"

#define RGB(r, g, b) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:1]
#define GREEN      RGB(  0, 202, 109)
#define RED        RGB(126, 16, 0)

@interface DetailViewController ()
@property (weak, nonatomic) IBOutlet UILabel *loanNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *loanDetailLabel;
@property (weak, nonatomic) IBOutlet UIImageView *chartImage;

@end

@implementation DetailViewController

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	
	self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
	self.navigationItem.title = self.loan.name;
	
	[self configureLoanDetail];
}

- (void)configureLoanDetail {
	
	UIColor *backgroundColor = self.loan.isGreen?GREEN:RED;
	self.view.backgroundColor = backgroundColor;
	
	[self.chartImage setImage:[UIImage imageNamed:self.loan.isGreen?@"chartUp":@"chartDown"]];
	[self.loanNameLabel setText:self.loan.name];
	
	if (self.loan.isGreen) {
		[self.loanNameLabel setTextAlignment:NSTextAlignmentRight];
		[self.loanDetailLabel setTextAlignment:NSTextAlignmentLeft];
	} else {
		[self.loanNameLabel setTextAlignment:NSTextAlignmentLeft];
		[self.loanDetailLabel setTextAlignment:NSTextAlignmentRight];
	}
	
	NSString *locationString;

	if (self.loan.location.town) {
		locationString = [NSString stringWithFormat:@"%@ (%@)", self.loan.location.town, self.loan.location.country];
	} else {
		locationString = self.loan.location.country;
	}
	
	NSString *detailString;
	
	if(self.loan.isGreen) {
		detailString= [NSString stringWithFormat:@"⏳ %@\n\n🎯 %@\n\n🏦 %@\n\n🌐 %@", self.loan.status, self.loan.activity, self.loan.sector, locationString];
	} else {
		detailString= [NSString stringWithFormat:@"%@ ⏳\n\n%@ 🎯\n\n%@ 🏦\n\n%@ 🌐", self.loan.status, self.loan.activity, self.loan.sector, locationString];
	}
	
	[self.loanDetailLabel setText:detailString];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)prefersStatusBarHidden {
	return YES;
}

@end
