//
//  DataController.h
//  OTFundraising
//
//  Created by Marc Güell Segarra on 16/10/15.
//  Copyright © 2015 Marc Güell Segarra. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Blocks.h"
#import "OTLoan.h"

@interface DataController : NSObject

-(id)initWithRealm:(RLMRealm *)realm;

- (void)fetchData:(ErrorBlock)block;

- (RLMResults<OTLoan *> *)getAllLoans;

@end