//
//  APIManager.h
//  OTFundraising
//
//  Created by Marc Güell Segarra on 16/10/15.
//  Copyright © 2015 Marc Güell Segarra. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Blocks.h"
#import <AFNetworking.h>

#define kRestUrl @"http://api.kivaws.org/v1/loans/search.json?status=fundraising"

@interface APIManager : NSObject

+ (void)fetchDataFromRestfulWS:(CompletionBlock)block;

@end