//
//  OTLoan.h
//  OTFundraising
//
//  Created by Marc Güell Segarra on 16/10/15.
//  Copyright © 2015 Marc Güell Segarra. All rights reserved.
//

#import <Realm/Realm.h>
#import "OTLoanLocation.h"

@interface OTLoan : RLMObject
@property NSString *name;
@property NSString *status;
@property NSString *activity;
@property NSString *sector;
@property OTLoanLocation *location;
@property BOOL isGreen;
@end

// This protocol enables typed collections. i.e.:
// RLMArray<OTLoan>
RLM_ARRAY_TYPE(OTLoan)