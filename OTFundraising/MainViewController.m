//
//  MainViewController.m
//  OTFundraising
//
//  Created by Marc Güell Segarra on 16/10/15.
//  Copyright © 2015 Marc Güell Segarra. All rights reserved.
//

#import "MainViewController.h"
#import "DataController.h"
#import "LoanTableViewCell.h"
#import <Realm/Realm.h>
#import "DetailViewController.h"
#import "Reachability.h"

@interface MainViewController () <UITableViewDataSource, UITableViewDelegate>

@property RLMRealm *realm;
@property DataController *dataController;
@property RLMResults<OTLoan *> *loansDataSource;
@property BOOL refreshInProgress;
@property BOOL mustShowPullCell;
@property Reachability* reach;
@end

#define kCellIdRight @"loanCellRight"
#define kCellIdLeft @"loanCellLeft"
#define kCellPullDown @"pullToRefreshCell"

@implementation MainViewController

- (id)initWithCoder:(NSCoder *)aDecoder {
	if ((self = [super initWithCoder:aDecoder]))
	{
		_realm = [RLMRealm defaultRealm];
		_dataController = [[DataController alloc] initWithRealm:_realm];
		_refreshInProgress = false;
		_mustShowPullCell = false;
	}
	return self;
}

- (void)initialCheck {
	_reach = [Reachability reachabilityWithHostname:@"www.google.com"];
	[_reach startNotifier];
	
	if(![_reach isReachable]) {
		_loansDataSource = [OTLoan allObjectsInRealm:_realm];
		[self.tableView reloadData];
	} else {
		[self fetchDataIntoDatasource:^{}];
	}
}

- (void)viewDidLoad {
	[super viewDidLoad];
	[self initialCheck];
	
	self.navigationItem.title = @"Loans";

	self.refreshControl = [[UIRefreshControl alloc] init];
	self.refreshControl.backgroundColor = [UIColor blackColor];
	self.refreshControl.tintColor = [UIColor whiteColor];
	[self.refreshControl addTarget:self
							action:@selector(getLatestLoans)
				  forControlEvents:UIControlEventValueChanged];
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
}

- (void)fetchDataIntoDatasource:(SimpleBlock)block {
	[_dataController fetchData:^(NSError *error) {
		_loansDataSource = [OTLoan allObjectsInRealm:_realm];
		
		[self.tableView reloadData];
		
		block();
	}];
}

- (void)getLatestLoans {
	
	if(!_refreshInProgress) {
		if(![_reach isReachable]) {
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No connection"
															message:@"Sorry, your internet connection is not available"
														   delegate:self
												  cancelButtonTitle:@"OK"
												  otherButtonTitles:nil];
			[alert show];
			
			[self.refreshControl endRefreshing];
		} else {
			_refreshInProgress = true;
			
			[self fetchDataIntoDatasource:^{
				[self updateRefreshControl];
			}];
		}
	}
}

- (void)updateRefreshControl
{
	if (self.refreshControl) {
		NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
		[formatter setDateFormat:@"MMM d, h:mm a"];
		NSString *title = [NSString stringWithFormat:@"Last update: %@", [formatter stringFromDate:[NSDate date]]];
		NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:[UIColor whiteColor]
																	forKey:NSForegroundColorAttributeName];
		NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title attributes:attrsDictionary];
		self.refreshControl.attributedTitle = attributedTitle;
		
		[self.refreshControl endRefreshing];
		
		_refreshInProgress = false;
	}
}

#pragma mark - UITableViewDataSource Datasource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	
	if([_loansDataSource count] > 0) {
		_mustShowPullCell = false;
		return [_loansDataSource count];
	} else {
		// It will exist almost one cell (for pull to refresh label purpose)
		_mustShowPullCell = true;
		return 1;
	}
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
	
	if(tableView == self.tableView) {
		
		if(_mustShowPullCell) {
			UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellPullDown];
			return cell;
		} else {
			if([_loansDataSource count] > 0) {
				OTLoan *currentLoan = [_loansDataSource objectAtIndex:indexPath.item];
				
				NSString *cellType;
				if(currentLoan.isGreen) {
					cellType = kCellIdRight;
				} else {
					cellType = kCellIdLeft;
				}
				
				LoanTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellType];
				[cell.loanNameLabel setText:currentLoan.name];
				
				return cell;
			} else {
				LoanTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellIdLeft];
				return cell;
			}
		}
		
	}
	
	return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
	DetailViewController *detailVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"DetailViewController"];
	
	detailVC.loan = [_loansDataSource objectAtIndex:indexPath.item];
	
	[self.navigationController pushViewController:detailVC animated:YES];
}

-(BOOL)prefersStatusBarHidden {
	return YES;
}

@end
