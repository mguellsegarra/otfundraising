//
//  OTLoanLocation.h
//  OTFundraising
//
//  Created by Marc Güell Segarra on 16/10/15.
//  Copyright © 2015 Marc Güell Segarra. All rights reserved.
//

#import <Realm/Realm.h>

@interface OTLoanLocation : RLMObject
@property NSString *countryCode;
@property NSString *country;
@property NSString *town;
@end

// This protocol enables typed collections. i.e.:
// RLMArray<OTLoanLocation>
RLM_ARRAY_TYPE(OTLoanLocation)
